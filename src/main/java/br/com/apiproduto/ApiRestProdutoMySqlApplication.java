package br.com.apiproduto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiRestProdutoMySqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiRestProdutoMySqlApplication.class, args);
	}

}
