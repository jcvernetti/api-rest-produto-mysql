package br.com.apiproduto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.apiproduto.entity.Product;
import br.com.apiproduto.services.ProductService;


//@Api(tags  = "Products")
@RestController
@RequestMapping(value= "/products")
public class ProductController {

	@Autowired
	private ProductService productsService;
	
//	@ApiOperation(value = "Create Product")
	@PostMapping
	public ResponseEntity<Product> createProduct(@RequestBody Product product) {
		Product newProduct = productsService.createProduct(product);
		return new ResponseEntity<>(newProduct, HttpStatus.CREATED);
	}
	
//	@ApiOperation(value = "List All")
	@GetMapping
	public ResponseEntity<List<Product>> ListAllProducts() {
		return new ResponseEntity<>(productsService.listAllProducts(), HttpStatus.OK);
	}
	
//	@ApiOperation(value = "Update Product")
	@PutMapping("/{id}")
	public ResponseEntity<Product> updateProduct(@RequestBody Product product, @PathVariable Long id) {
		return new ResponseEntity<>(productsService.updateProducts(product, id), HttpStatus.OK);
	}
	
//	@ApiOperation(value = "Delete Product")
	@DeleteMapping("/{id}")
	public ResponseEntity<String> deleteProduct(@PathVariable Long id) {
		productsService.deleteProduct(id);
		return ResponseEntity.ok().build();
	}
}
